Source: liborlite-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           Angel Abad <angel@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: libclass-xsaccessor-perl <!nocheck>,
                     libdbd-sqlite3-perl <!nocheck>,
                     libdbi-perl <!nocheck>,
                     libfile-remove-perl <!nocheck>,
                     libparams-util-perl <!nocheck>,
                     libtest-deep-perl <!nocheck>,
                     libtest-script-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liborlite-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liborlite-perl.git
Homepage: https://metacpan.org/release/ORLite
Rules-Requires-Root: no

Package: liborlite-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdbd-sqlite3-perl,
         libdbi-perl,
         libfile-remove-perl,
         libparams-util-perl
Suggests: libclass-xsaccessor-perl
Description: lightweight SQLite-specific ORM
 ORLite is a Perl module that implements an object-relational mapper designed
 specifically for SQLite. It follows many of the same principles as the ::Tiny
 series of modules and has a design that aligns directly to the capabilities
 of SQLite.
 .
 ORLite discovers the schema of a SQLite database, and then deploys a set of
 packages for talking to that database.
